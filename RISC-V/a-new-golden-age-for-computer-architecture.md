# 《计算机体系结构的新黄金时代》

([A New Golden Age for computer Architecture](https://cacm.acm.org/magazines/2019/2/234352-a-new-golden-age-for-computer-architecture/fulltext))  
https://cacm.acm.org/magazines/2019/2/234352-a-new-golden-age-for-computer-architecture/fulltext

中文译文已经移除，请参看以下版权信息，详见以上原文链接.

----

©2019 ACM 0001-0782 / 19/02

如果不为牟利或商业利益而制作或分发副本，并且副本在第一页上带有此声明和全文引用，则可以免费提供为个人或教室使用而为个人或教室使用而制作此部分或全部作品的数字或纸质副本的许可。必须尊重非ACM拥有的本作品组件的版权。允许使用信用摘要。若要进行其他复制，重新发布，在服务器上发布或重新分发到列表，则需要事先获得特定的许可和/或费用。从Permissions@acm.org或传真（212）869-0481请求发布权限。

数字图书馆由计算机协会发布。版权所有©2019 ACM，Inc.
