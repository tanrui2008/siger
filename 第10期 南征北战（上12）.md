《南征北战》[目录](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98)：
- 一、封面故事：[阳光海涛](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E4%B8%80%E5%B0%81%E9%9D%A2%E6%95%85%E4%BA%8B%E9%98%B3%E5%85%89%E6%B5%B7%E6%B6%9B)
  - [能在社区传播的路上有个我](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#-%E8%83%BD%E5%9C%A8%E7%A4%BE%E5%8C%BA%E4%BC%A0%E6%92%AD%E7%9A%84%E8%B7%AF%E4%B8%8A%E6%9C%89%E4%B8%AA%E6%88%91)
  - [从零开始在Windows下启动openEuler（WSL）](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#-%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E5%9C%A8windows%E4%B8%8B%E5%90%AF%E5%8A%A8openeulerwsl)
- 二、[从 0 开始构建 RISC-V 私有云](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E4%BA%8C%E4%BB%8E-0-%E5%BC%80%E5%A7%8B%E6%9E%84%E5%BB%BA-risc-v-%E7%A7%81%E6%9C%89%E4%BA%91)
  - 见证社区成长：[优矽科技、芯来科技 加入 openEuler](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E8%A7%81%E8%AF%81%E7%A4%BE%E5%8C%BA%E6%88%90%E9%95%BF%E4%BC%98%E7%9F%BD%E7%A7%91%E6%8A%80%E8%8A%AF%E6%9D%A5%E7%A7%91%E6%8A%80-%E5%8A%A0%E5%85%A5-openeuler)
  - [SUMMER 2021 & NutShell](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#-summer-2021--nutshell)
- 三、[一生一芯 NutShell 国科大](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%AD3%EF%BC%89.md)
- 四、[欧拉指北 从 0 到 10](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%9B%9B%E6%AC%A7%E6%8B%89%E6%8C%87%E5%8C%97-%E4%BB%8E-0-%E5%88%B0-10)
- 五、[SIG组生态树 73-81 SIG 治理地图 生机盎然](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E4%BA%94sig%E7%BB%84%E7%94%9F%E6%80%81%E6%A0%91-73---81-sig-%E6%B2%BB%E7%90%86%E5%9C%B0%E5%9B%BE-%E7%94%9F%E6%9C%BA%E7%9B%8E%E7%84%B6)
  - [社区成立73个特别兴趣小组（SIG）](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E7%A4%BE%E5%8C%BA%E6%88%90%E7%AB%8B73%E4%B8%AA%E7%89%B9%E5%88%AB%E5%85%B4%E8%B6%A3%E5%B0%8F%E7%BB%84sig)
  - [17 sigs not in table:](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#17-sigs-not-in-table)
  - 大作业：[北向 sig-minzuchess 南向 sig-riscv](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%A4%A7%E4%BD%9C%E4%B8%9A%E5%8C%97%E5%90%91-sig-minzuchess-%E5%8D%97%E5%90%91-sig-riscv)
- 六、[用 RISC-V 赋予的全栈能力，打破条条框框，轻装前进！](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%85%AD%E7%94%A8-risc-v-%E8%B5%8B%E4%BA%88%E7%9A%84%E5%85%A8%E6%A0%88%E8%83%BD%E5%8A%9B%E6%89%93%E7%A0%B4%E6%9D%A1%E6%9D%A1%E6%A1%86%E6%A1%86%E8%BD%BB%E8%A3%85%E5%89%8D%E8%BF%9B)

---

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/102704_3fdc052f_5631341.jpeg "南征北战（欧拉）900.jpg")

> “快搬小马扎占位啦！”，随着小伙伴儿的招呼，炎炎夏日里的最佳活动————露天电影就要开演了。这个伴随童年的文化活动给我留下了很多美好的回忆。通常露天电影的放映场地，是一片空场，白天定是孩子们嬉戏的场所，撞拐，弹球，这些怀旧的桥段，偶尔还会出现在现在的自媒体小视频中。今天，就借着这幅白描的民间插画，开篇啦。 @yuandj  2021-5-8

今天的影片是《南征北战》，看电影幕布的标题，可是100%从影片上截取的。尽管制作这期海报前后历时近一个月，但这份精雕细琢，也随着剧情的主人公 “铁牛” 的觉悟，也让我对整个 openEuler 社区有了更加精进的理解，无论是宏观还是微观，我都像穿越到了那个激情燃烧的岁月里一样，和战士们一起成长啦。

# 《[南征北战](https://gitee.com/openeuler/RISC-V/issues/I3CEEF#note_4726513_link)》

@zhoupeng01 @whoisxxx
> 未来几年，将是各种 RISC-V 硬件百花齐放的时代，openEuler RISC-V SIG 将致力于丰富 RISC-V 的 **北向** 软件生态，联合 RISC-V 的 **南向** 硬件生态，将 openEuler RISC-V 打造成一个开源的基础软件平台。欢迎大家关注 RISC-V SIG。

这段话是节选自 《[openEuler RISC-V 背后的故事](https://my.oschina.net/openeuler/blog/4955967)》2-18 中[周鹏](https://gitee.com/zhoupeng01)和[张旭舟](https://gitee.com/whoisxxx)两位老师语录，看两个加粗的关键词 “北向” “南向” 它们将贯穿本篇始终，成为我带领同学们探索整个 openEuler 社区的导引。这句话也收录在了我的多个[学习笔记](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5030648_link) [issues](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_4621395_link) 中, 我把它称作 [sig-RISC-V](https://gitee.com/openeuler/RISC-V) 的[愿景](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_4621395_link) 。这段话的语境完美诠释了 2020-12-27 初识 openEuler 时，台上演讲者口中反复强调的 “南向” ，彼时它成为了硬件生态的代名词。就像是《[南征北战](https://gitee.com/openeuler/RISC-V/issues/I3CEEF#note_4726513_link)》里满腹牢骚的战士的 “蒙圈” 状态。这也是 SIGer 创刊号 #0 号期刊 《[Hello,openEuler!](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC0%E6%9C%9F%20Hello,%20openEuler!%20%E6%8B%A5%E6%8A%B1%E6%9C%AA%E6%9D%A5.md)》欧来指北的动因，当时作为一名小兵，急切地需要解开思想上的结，一种渴望被拥抱的赶脚，在探索 RISC-V 的过程中 晓旭老师的 “秒回” 让我记忆深刻，这成就了 [# 4](https://gitee.com/flame-ai/siger/pulls/9) 期《[Better RISC-V For Better Life](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC4%E6%9C%9F%20Better%20RISC-V%20For%20Better%20Life.md)》，如果本期是 RISC-V 专题的进阶篇，它就是本篇的兄弟篇啦。也是被[社区收录](https://gitee.com/openeuler/RISC-V/blob/master/README.md#%E5%8A%A8%E6%80%81)的第一篇 SIGer 期刊。

> 1947年冬，国民D军队进攻华东解放区，我军为有效歼敌，作战略后撤。对此，部队与百姓中有好些人思想不通。为保证战事胜利，高营长和女村长赵玉敏分别向群众作解释。敌人误以为我军不敌败走，集中三十万人马，从三面合围。我军在大沙河阻击敌人六昼夜，然后至凤凰山围歼已被包围的国民D李军长所属七个师。高营长奉命抢占摩天岭高地，以阻击前来增援的国民D张军长所属部队，并全歼李军长所属部队，活捉了李国长。接着，高营长又率部队乘胜追击张军长所属部队。赵村长率民兵配合主力，断敌退路。陷于穷途末路的张军长所属部队企图以 **炸** 毁水坝阻挡我军，赵玉敏带领游击队切断水坝上的 **炸** 药导火线，保拄了水坝。一场鏖战，全歼张军长所属部队，生俘张军长等高级将领。

上海电影制片厂摄制于1952年的《[南征北战](https://gitee.com/openeuler/RISC-V/issues/I3CEEF#note_4726513_link)》，是新中国第一部军事影片，上面是故事梗概。该片取材于解放战争中华东战场的一个战例，表现了敌强我弱的形势下，正确运用运动战的战略思想，“ **集中优势兵力，各个歼灭敌人** ”消灭敌人取得胜利的过程。这多么符合当前 “以开源突围” 的实战场景啊。经过认真地 “[拉片](https://gitee.com/flame-ai/hello-openEuler/issues/I3I5TZ)” 整理出印象深刻的  **[7](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)**  个场景。本期的主题，在一个月前就确定啦，今天就是交卷的时刻。一边放映影片，一边旁白我对 openEuler 社区的理解。

## 一、封面故事：阳光海涛

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/143713_38a72560_5631341.png "屏幕截图.png")](https://gitee.com/openeuler/RISC-V/issues/I3CEEF#note_4726513_link) 

> 好样的铁牛！荧幕经典，激励无数少年人。最后1分钟真正的高潮，战略大反攻的时刻就要到来啦。

这是《[南征北战](https://gitee.com/openeuler/RISC-V/issues/I3CEEF#note_4726513_link)》影讯最后一幕，战略大反攻时的高光时刻，整个故事的主角，年轻阳光的战士————铁牛（上影的老艺术家，在建国初期塑造了相当多的荧幕人物，士兵、工人，还有家喻户晓的《西游记》中的弥勒佛）。我已经将他的艺名和银幕里的角色合二为一啦，在电影资料中，这个普通的战士，甚至没有名字，被 “小胖子” 替代。这名战士最大的特点，和其他牢骚满腹的战士的鲜明对比，坚决执行命令，在还不能完全领略首长的战略意图的时候，依然信心满满地说服其他同志。并在整个故事中，担当积极正面的角色，也是结尾高潮特写的 C 位担当。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/122820_ef278fc4_5631341.png "屏幕截图.png") @[王海涛](https://gitee.com/ouyanghaitao)

他就是我向您推荐的华为实习生，被称为 “阳光海涛” 的新同学 @[ouyanghaitao](https://gitee.com/ouyanghaitao)。和平日里的社区工作邮件不同，海涛的行文流露出的就是 “铁牛” 一般的气质，正能量满满，认真完成每一项任务。这让我回想起正在进行时的 [SUMMER 2021](https://mp.weixin.qq.com/s?__biz=MzI2NDE4OTE2Mg==&mid=2247488178&idx=1&sn=db93979d09ee783f4694eda7f7506fc0) 的开源之夏活动，正有越来越多的年轻人，走向 openEuler 社区，而就在本期聚焦的 sig-RISC-V 也在上月刚刚收录了一篇实习生的文章。每每见到这样朝气蓬勃的年轻人，没有老师不会心动，要尽力推他一把，这就是本期封面人物的由来。

> 真情分享，赤子之心，就是我对 “阳光海涛” 的评价，这篇自述通过他亲笔的鲜活文字为我们勾画了一副青年人朝气蓬勃追寻梦想的样貌。自 SIGer 创刊以来，非常欣蔚能遇到这一颗颗年轻的心，他们的热情不只激励了我，也一定能引领更多的青少年，投身到一条奔向阳光之路。全篇共分三个部分：
> 1. 青涩的中学时代，也是恋爱的季节。用了三个段落... 是一个老师家长眼中的好孩子
> 2. 别样的大学时代，不负韶华。同样是三个段落... 从关键词 ACM 竞赛，人工智能，到拥抱操作系统
> 3. 展望未来，做时代的弄潮儿。结尾为我们展示了他的坚定，也是每一个阳光少年都有的风貌。

### • 能在社区传播的路上有个我

《[【人物】阳光海涛](https://gitee.com/yuandj/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E4%BA%BA%E7%89%A9%E3%80%91%E9%98%B3%E5%85%89%E6%B5%B7%E6%B6%9B.md)》.md 就是我撰写序言的标题，是他的自我介绍。它缘起于我对他的采访邀请，“[能在社区传播的路上有个我](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_5015041_link)” 是我收到他接受采访的回信中提炼出来的主题。一个朝气蓬勃的青年，不负韶华的劲头。

### • 从零开始在Windows下启动openEuler（WSL）

这样的年轻人依靠的不是推荐信，而是他的实力担当 [from zero to startup openEuler under Windows (WSL).md](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md) 这也是第一次见到海涛时，他的社区任务。我清晰地记得他的邮件，中英文对照，认真阐述每一个自己不明白的问题，所有老师都认真回复，这封没有半点拖泥带水的邮件，不像初入社区的我 “包袱心” 那么重，怕被叫成实习生的尴尬。相信 WSL 方案能为同学们进入社区大开方便之门，下面的提纲：

> 亲爱的读者朋友你们好。我叫王海涛，一名大三计算机学生，也是华为openEuler开发与运营部门实习生。  
> 今天从0开始，与你一起在Windows下启动openEuler。  
> 由于是从0开始，面向对象为0基础的小白，所以会讲多一些基本知识。  
> 本文大致分为以下几个部分，**可选择性阅读**。
> 
> 1. [什么是openEuler](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md#%E4%BB%80%E4%B9%88%E6%98%AFopeneuler)
> 2. [为什么要用openEuler](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md#%E4%B8%BA%E4%BB%80%E4%B9%88%E8%A6%81%E7%94%A8openeuler)
> 3. [如何安装使用一个Linux发行版](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md#%E5%A6%82%E4%BD%95%E5%AE%89%E8%A3%85%E4%BD%BF%E7%94%A8%E4%B8%80%E4%B8%AAlinux%E5%8F%91%E8%A1%8C%E7%89%88)
> 4. [什么是WSL](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md#wsl%E6%98%AF%E4%BB%80%E4%B9%88)
> 5. [如何使用WSL启动openEuler](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md#%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8wsl%E5%90%AF%E5%8A%A8openeuler)
>    1. [Windows下控制台的使用](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md#windows%E4%B8%8B%E6%8E%A7%E5%88%B6%E5%8F%B0%E7%9A%84%E4%BD%BF%E7%94%A8)
>    2. [安装WSL](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md#%E5%AE%89%E8%A3%85wsl)
>    3. [安装openEuler](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md#%E5%AE%89%E8%A3%85openeuler)
>    4. [Linux下基本命令行的使用](https://gitee.com/flame-ai/hello-openEuler/blob/master/SIGer/from%20zero%20to%20startup%20openEuler%20under%20Windows%20(WSL).md#linux%E4%B8%8B%E5%9F%BA%E6%9C%AC%E5%91%BD%E4%BB%A4%E8%A1%8C%E7%9A%84%E4%BD%BF%E7%94%A8)

## 二、[从 0 开始构建 RISC-V 私有云](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5030648_link)

再一个从 0 构建，可见作为社区是多么渴望拥抱新人啊，那为什么会有这样一个称号给到小白 “实习生”，然后，展示给同学们的又都是千挑万选出来的 阳光海涛 呢？这只能是插曲，并不是本刊的主题。

> 剧情：在一营营部，赵玉敏和高营长坐在桌旁一边交谈着，一边认真地做着笔记。  
> 赵玉敏：起先哪，我就是担心群众思想转不过弯来，扯战士们的腿，耽误你们行动。  
> 高营长：那你就把我刚才的话给家属们解释解释嘛。  
> ...  
> 赵玉敏：我工作上的缺点哪，实在太多了，也不知怎么弄的？一碰到困难就想不到办法来。  
> 高营长：你比从前进步的多了  
> 赵玉敏：你别表扬了，再进步也赶不上你们哪！（站了起来）你刚才的话，真像是一把钥匙，把我思想上的圪垯一下就给我解开了。我开会去了。  
> 剧情：赵玉敏说着便将笔记本和笔装进了口袋里。  
> 高营长：好吧！只要群众们思想搞通了，那我们部队就好带得多了。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/230933_b4ca70fa_5631341.png "在这里输入图片标题")](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)

- （[中集](http://blog.sina.com.cn/s/blog_e781b75e0103076j.html)） ： 游戏队长是地方组织的带头人，她的思想没有转变过来，是没法带队伍的，高营长的思想工作起了效，为后面结尾时游击队策应大部队保护堤坝，打狙击骚扰敌军撤退埋下了伏笔。贯穿始终的统一思想的重要性。一个茶缸子，和后面敌军的酒会（作战会议）形成了鲜明的对比。

这是《[南征北战](https://gitee.com/openeuler/RISC-V/issues/I3CEEF#note_4726513_link)》故事要点：场景（[7个](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)）中的一个。被放置在这里播放，是说明 “思想工作” 的重要性，因为高营长的这次细致入味的分析和解答，为后面游击队主动响应大部队，甚至决胜关键环节 “粉碎” 敌人炸坝突围的阴谋，起到了关键作用。这就是主观能动性对于大局的作用。这就是本节标题，再一个 “从 0 开始” 的价值，它贯穿了探索社区全局的始终，也因为这个发源，以及这个拥抱，带着这个问题的探索，由点到面，再到全局，让我完成了这篇作业，呈递上一片 RISC-V 主题的《南征北战》。再次感谢 [周鹏](https://gitee.com/zhoupeng01)和[张旭舟](https://gitee.com/whoisxxx) 两位老师。

> 这个提议挺好，很用心，感谢。  
> 乐观估计2021年国产高性能的RISC-V SoC会有大发展，在硬件上会提供更强的计算能力和丰富的外设接口，这会为RISC-V软硬件生态发展创造更好条件。目前openEuler RISC-V软件包也基本支持云服务相关的基本软件包的移植。

@zhoupeng01 [RPMbuild_RISC-V.md 0 byte 会列出哪些软件包？](https://gitee.com/openeuler/RISC-V/issues/I3CEEF#note_4570525)
1. 搭建LAMP环境做个人网站、
2. 编写脚本做无情的爬虫机器、
3. 接入ASR/TTS在线接口做私人语音助手、
4. 安装frp做内网穿透、
5. 跑MQTT broke来作为智能家居网关中心

> @zhoupeng01 @whoisxxx 能有两位老师鼎立支持，此愿必成  :pray:  :heart:  :+1:

然后，就是 1个月 [14 comments](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5030648_link) 的学习，和【专题】筹备：#[I3CHPZ](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ) 

- 第 10 期，也是一份圆满，十全十美 , 十代表的是轮回 本刊发愿  :pray: 
- sig-RISC-V 的[愿景](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_4621395_link) 丰富 RISC-V 的北向软件生态，联合 RISC-V 的南向硬件生态
- sig [组内分工](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_4624651_link) & [硬件瓶颈](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_4627355_link)
- [如何在openEuler树莓派镜像上部署k8s+iSula集群](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_4628837_link)
- 原创 [树莓派openEuler首次启动](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_4628959_link)
- [各种Linux发行版本如何安装软件包？](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_4629637_link)

### 见证社区成长：优矽科技、芯来科技 加入 openEuler

从发愿托付给社区，到从社区中学习，有幸见证社区发展，是社区伴随生态（市场）发展的结果，身为社区中的一份子，每个愿的汇集本身，就是生态成长的形态，身在其中，享受当下，见证生态繁荣。

- [RISC-V 处理器开发商优矽科技加入 openEuler 社区](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_4752587_link)
- [芯来科技加入openEuler开源社区，助力开源生态建设](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5015053_link)

### • SUMMER 2021 & NutShell

温故而知新，已经不适合社区这个 “大课堂” 啦，随时随地学习，随时随地重温，一片片知识地图的拼图逐渐聚合成一个完整的链条，如我一样，每个社区成员都会逐渐丰富自己的根系去汲取社区的知识营养，并作为生态的一部分回补社区，这就是一个动态的生态平衡，而它的动力，就是一个个待实现的愿望。

- [如何在NutShell(果壳, UCAS) COOSCA1.0上部署测试openEuler OS](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5016729_link)
& PYNQ-Z2零基础学习详解
- NutShell (果壳) : [README.md](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5016741_link) 3天前  & Demo: [Running Debian on FPGA](https://images.gitee.com/uploads/images/2021/0503/232102_c1847bb4_5631341.gif)

今天我可以自豪的说，RISC-V sig 的 README.md 的每一个字我都能理解啦，这在1个月前，大部分关键词还只能被忽略，或者暂时记下，留待以后查详。下面这篇，就是我曾经委托给老师的问题，如今，我觉得，我可以自己解答啦。

- 从 0 开始构建 RISC-V 私有云：[reREading sig-RISC-v](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5023623_link)

> 从2个月前的《新康众开源》关注到 RISC-V 新成员裴老师入驻社区，到1个月前开始撰写《RISC-V》日志并立下这个 FLAG。前后三个月时间，一幅隐隐约约的 RISC-V 地图已经浮现在了眼前，《从 0 开始构建 RISC-V 私有云》就是答卷，要由我亲自撰写，才能交上这份答卷。没有做过学生，怎么能做 “老师” ，学长都担当不起。
> - reREading sig-RISC-v 阅读，反复阅读，更多的关键词能够理解，这就是学习的长进。
> - documents: iSulad_build_guide_riscv.md
> - tools
> - 基于 RV 的私有云还有多远，已有了答案
>   1. 选在一块具备条件的硬件卡
>   2. 安装好容器
>   3. 装备上 LAMP
>   4. 装上WP，开通个站

如果说 阳光海涛 只是有一个 “实习生” 的名号，@[shentalon](https://gitee.com/shentalon) 建立 iSulad_build_guide_riscv.md 的全过程，都在他的工作仓中一览无余。

> documents: 又一位 summer2020 的实习生 将 iSulad 容器引擎移植到了 RV 环境中。它是一个网红（iSula 是一种云原生轻量级容器解决方案），这就是 从0构建的范本，私有云可以如法炮制。也是距离最近的一个方案啦。

- https://gitee.com/shentalon ：[iSulad_build_guide_riscv.md](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5026052_link) 作者长成记
- workspace [this](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5030648_link).
